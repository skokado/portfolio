# 自己紹介

![me](assets/images/logo-240x240.jpg)

| Key      | Value                                             |
| -------- | ------------------------------------------------- |
| なまえ   | Shota Kokado                                      |
| 生年月日 | 1990年8月                                         |
| 在住     | 埼玉県                                            |
| 現職     | インフラエンジニア / サーバーサイド開発エンジニア |


## 略歴

| 期間                  | 職種                                                                                             |
| --------------------- | ------------------------------------------------------------------------------------------------ |
| 2023年1月～(現在)     | サーバーサイドエンジニア@[RevComm](https://www.revcomm.co.jp/){target=_blank} |
| 2022年1月～2022年12月 | インフラエンジニア@RevComm                                                                       |
| 2019年8月～2021年12月 | サーバサイドエンジニア@AI/データ分析ベンチャー                                                   |
| 2016年4月～2019年7月  | インフラエンジニア@ネット金融企業                                                                |
| 2013年～2016年3月     | インフラエンジニア@SIer                                                                          |

## 外部リンク/アカウント

- [GitHub](https://github.com/skokado){target=_blank}
- [Qiita](https://qiita.com/skokado){target=_blank}
- [LAPRAS](https://lapras.com/public/skokado){target=_blank}

---

## Blogs

- 2023/04/14: [Django 4.2LTSがリリースされたので早速アップグレードした話](https://tech.revcomm.co.jp/upgrade-django4.2){target=_blank}
- 2023/02/14: [Amazon Inspectorによるプラットフォーム診断とコンテナイメージ改善の取り組み - RevComm Tech Blog](https://tech.revcomm.co.jp/platform-inspection-with-amazon-inspector){target=_blank}
- 2022/12/13: [RevComm の Django アプリケーションとチーム開発について紹介します - RevComm Tech Blog](https://tech.revcomm.co.jp/django-repository-in-miitel-analytics){target=_blank}
- 2022/10/18: [PyCon JP 2022参加レポート（RevCommのエンジニア2名が登壇しました） - RevComm Tech Blog](https://tech.revcomm.co.jp/report-pyconjp-2022){target=_blank}
- 2022/04/22: [AWS 特権 ID の使用を Slack に通知する - RevComm Tech Blog](https://tech.revcomm.co.jp/2022/04/22/notify-aws-console-login/){target=_blank}

## Slides

- [PythonにとってのよりよいDockerfileを考える - Speaker Deck](https://speakerdeck.com/revcomm_inc/pythonnitotutenoyoriyoidockerfilewokao-eru){target=_blank}

## スキル

### Python

2017年～

- Webアプリケーションフレームワーク
    - **Django**
    - FastAPI(Flask)
- クローラ/スクレイピング
    - requests
    - BeautifulSoup
    - Scrapy
- Numpy/Pandas
- 副業
    - Webスクレイピング/データ収集系<br>
    - Webアプリ開発(Django)

### フロントエンド

- React
    - Next.js

### DevOps/IaC

- コンテナ
    - Docker
    - Amazon ECR/AWS Fargate
- CI/CD
    - GitLab CI/CD
    - GitHub Actions
    - AWS Codeシリーズ
- 構成管理/IaC
    - Terraform
    - CloudFormation
    - Ansible

### インフラ

2013年～2018年

- サーバ(Linux)
- ネットワーク
    - Cisco
    - Juniper
    - Fortigate
    - F5
    - A10

## 職務経歴

### RevComm(2022年1月 ～)

※TBW

### データ分析ベンチャー(2019年8月 ～ 2021年12月)

- データ分析基盤構築
- 機械学習プロダクト企画、開発

### ネット金融企業(2016年4月 ～ 2018年7月)

- 新規プロダクトのインフラ設計、ベンダコントロール
    - 複数データセンター間のフェールオーバー設計、構築
- オンプレシステムのクラウド移行
- 各種ベンダコントロール、社内調整
- プロダクト運営

### SIer(2013年 ～ 2016年3月)

- オンプレシステムのネットワーク・サーバ構築、運用、保守
