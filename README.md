## Tips

### スタイルのカスタマイズ

GitHub ライクなテーマを使用する。

Markdown の先頭に下記を追加

※"---"も含める

```markdown
---
stylesheet: https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css
body_class: markdown-body
---
```

### ヘッダー/フッターの追加

```markdown
---
pdf_options:
  format: A4
  margin: 30mm 20mm
  displayHeaderFooter: true
  headerTemplate: |-
    <style>
      section {
        margin: 0 auto;
        font-family: system-ui;
        font-size: 11px;
      }
    </style>
    <section>
      <span class="date"></span>
    </section>
  footerTemplate: |-
    <section>
      <div>
        Page <span class="pageNumber"></span>
        of <span class="totalPages"></span>
      </div>
    </section>
---
```

### 改ページの挿入

<!-- ここで改行 -->
<div style="page-break-before:always"></div>
